FROM ubuntu:22.04

RUN apt-get update

RUN apt-get install -y mc apache2

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
